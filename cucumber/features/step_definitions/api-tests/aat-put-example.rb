Given(/^I to do PUT on clients endpoint$/) do
  $aat_api.put_example
end

Then(/^the client is changed with success$/) do
  $aat_api.put_confirm_data_change
end
